<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function create(){
        return view ('create');
    }

    public function handlePost(Request $request){

        //dd($request->all());
        $fn = $request['firstname'];
        $ln = $request['lastname'];

        return view('welcome', compact('fn','ln'));
    }
}

